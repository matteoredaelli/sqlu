# sqlu

sqlu is a simple tool for transforming or extracting info from sql statements

## installation

pip install sqlu

## Useful functions

- remove_comments: remove comments from a sql statement
- extract_tables_from_select: extract tables/views list from a select statement